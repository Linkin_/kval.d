class Client < ActiveRecord::Base
  has_many :ad_users, dependent: :destroy
  has_many :components, dependent: :destroy
  has_many :permissions, dependent: :destroy
  validates :name, presence: true, length: {maximum: 70}
  validates :descr, length: {maximum: 500}


end
