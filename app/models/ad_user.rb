class AdUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :client
  validates :name, presence: true, length: {maximum: 70}
  validates :client_id, presence: true
  validates :active_directory, presence: true, length: {maximum: 70}
  validates :username, presence: true, length: {maximum: 70}
  validates :password, presence: true

  def auto_display
    "#{self.name} (#{self.active_directory})"
  end
end
