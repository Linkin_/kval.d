class Parameter < ActiveRecord::Base
  belongs_to :component
  validates :name, presence: true, length: {maximum: 70}
  validates :value, presence: true, length: {maximum: 70}
  validates_presence_of :component
end
