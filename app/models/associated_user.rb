class AssociatedUser < ActiveRecord::Base
  belongs_to :component
  validates_presence_of :username
  validates_presence_of :password
  validates_presence_of :component
  validates :name, length:  {maximum: 70}
end
