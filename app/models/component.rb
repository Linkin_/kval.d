class Component < ActiveRecord::Base
  belongs_to :client
  has_many :parameters, dependent: :destroy
  has_many :associated_users, dependent: :destroy
  has_many :permissions, dependent: :destroy
  validates :name, presence: true, length: {maximum: 70}
  validates :descr, length: {maximum: 350}
  acts_as_tree
end
