class Permission < ActiveRecord::Base
  belongs_to :user
  belongs_to :client
  belongs_to :component
  validates_presence_of :user_id

end
