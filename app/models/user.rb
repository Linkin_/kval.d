class User < ActiveRecord::Base
  has_many :ad_users
  has_many :permissions, dependent: :destroy
  attr_accessor :remember_token
  before_save {self.email = email.downcase}
  validates :name, presence: true, length: {maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255},
            format: {with: VALID_EMAIL_REGEX},
            uniqueness: {case_sensitive: false}
  validates :password, length: { minimum: 6 }, allow_blank: true
  has_secure_password

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Returns clients where user with provided id is editor
  def self.show_clients(id)
    permissions = User.clients_by_user(id)
    client = [ ]
    permissions.each do |record|
      client << Client.find_by(id: record.client_id)
    end
    client
  end

  # Returns all permissions where user is client editor
  def self.clients_by_user(id)
    clients = Permission.where(["user_id = ? AND component_id IS NULL ", id])
  end

  # Returns components where user with provided id is viewer
  def self.show_components(id)
	  permissions = User.components_by_user(id)
		components = [ ]
		permissions.each do |record|
			components << Component.find_by(id: record.component_id)
		end
		components
  end
  # Returns all permissions where user is component viewer
  def self.components_by_user(id)
    components = Permission.where(["user_id = ? AND client_id IS NULL ", id])
  end

  # Returns array with clients ids of components provided
	def self.clients_pool (comp)
		pool = [ ]
		comp.each do |record|
			pool << Client.find_by(id: record.root.client.id)
		end
		pool
	end

  # Returns array with root component ids of components provided
	def self.component_roots (comp)
		comp_ancestors = []
		comp.each do |record|
			comp_ancestors << record.root
		end
		comp_ancestors
	end

  # Returns array of all ancestor components of components provided
  def self.component_ancestors (comp)
	  comp_ancestors = []
	  comp.each do |record|
		  comp_ancestors << record.self_and_ancestors_ids
	  end
	  ancestors = []
	  comp_ancestors.each do |row|
		  row.map do |a|
			  ancestors << a
		  end
	  end
		ancestors
  end

   # Returns all root components of client
  def self.components_by_client(id)
	  components = Client.find_by(id: id).components
  end
end
