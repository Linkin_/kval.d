// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require autocomplete-rails
//= require bootstrap
//= require turbolinks
//= require_tree .

$('#tabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
});

$('#tabs a[href="#tabs-1"]').tab('show');
$('#tabs a[href="#tabs-2"]').tab('show');
$('#tabs a[href="#tabs-3"]').tab('show');