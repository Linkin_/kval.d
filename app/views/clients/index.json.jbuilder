json.array!(@clients) do |client|
  json.extract! client, :id, :name, :descr
  json.url client_url(client, format: :json)
end
