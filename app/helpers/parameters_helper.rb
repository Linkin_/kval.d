module ParametersHelper
  def editor_param
    parameter = Parameter.find(params[:id])
    component = parameter.component
    client = component.root.client.id
    value = false
    user = Permission.find_by(client_id: client, user_id: current_user)
    if !user.nil?
      value = true
    end
    value
  end
end