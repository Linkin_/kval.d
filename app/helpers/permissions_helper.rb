module PermissionsHelper
  def editor_permission
    value = false
    if !(params[:id]).blank?   # Check whether permission in being created. If params[:id] is not blank permission
      # exists.
      perm = Permission.find(params[:id])
      component = Component.find_by(id: perm.component_id)
      client = Client.find_by(id: perm.client_id)
      if component.blank?
        perm = Permission.find_by(client_id: client.id, user_id: current_user)
        if !perm.blank?
          value = true
        end
      else
        component_client = component.root.client.id
          user = Permission.find_by(client_id: component_client, user_id: current_user)
          if !user.nil?
            value = true
          end
      end
    else
      client = Client.find_by(id: params[:client_id])
      component = Component.find_by(id: params[:component_id])
      if !component.blank?
          component_client = component.root.client.id
          user = Permission.find_by(client_id: component_client, user_id: current_user)
          if !user.nil?
            value = true
          end
        else
          perm = Permission.find_by(client_id: client.id, user_id: current_user)
          if !perm.blank?
            value = true
          end
      end
    end
    value
  end

  def is_perm_editor
    redirect_to (root_url) unless current_user.admin? || editor_permission
  end
end

