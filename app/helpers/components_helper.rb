# Generates HTML code from rendering components component tree
module ComponentsHelper
  def components_tree_for(components)
    components.map do |component, nested_components|
      if !(nested_components.nil?)
      render(component) +
          (nested_components.count > 0 ? content_tag(:ul, components_tree_for(nested_components)) : nil)
        end
    end.join.html_safe
  end

  # Generates HTML code from rendering components component tree
  # Takes in account only those components, that has id that can be found in array "ancestors"
  def components_tree_for_user_show(components, ancestors)
	  components.map do |component, nested_components|
		  if !(nested_components.nil?)
				if ancestors.include?component.id
				  render(component) +
						  (nested_components.count > 0 ? content_tag(:ul, components_tree_for_user_show(nested_components,ancestors)) : nil)
				end
		  end
	  end.join.html_safe
  end

  # Determines whether user is allowed to see component as a viewer
  def user
    component = Component.find(params[:id])
    components = component.self_and_ancestors_ids
    value = false
    components.each do |component|
      user = Permission.find_by(component_id: component, user_id: current_user)
      if !user.nil?
        value = true
        break
      end
    end
    value
  end

  # Determines whether user is allowed to interact with component as client editor
  def editor_comp(comp = '')
    id = params[:id]
    parent_id = params[:parent_id]
    comp_id = params[:component_id]
    if comp.blank?
      if id.nil?
        if parent_id.nil?
          component = Component.find_by(id: comp_id)
        else
          component = Component.find_by(id: parent_id)
        end
      else
        component = Component.find_by(id: id)
      end
    else
      component = Component.find_by(id: comp)
    end

    client = component.root.client.id
    value = false
    perm = Permission.find_by(client_id: client, user_id: current_user)
    if !perm.nil?
      value = true
    end
    value
  end

  # Determines whether user is admin or client editor
  def is_editor_comp
    redirect_to(root_url) unless editor_comp | current_user.admin?
  end
  def user_access
    redirect_to(root_url) unless user | current_user.admin? | editor_comp
  end

end
