module ClientsHelper
  def editor_clients
    id = params[:id]
    client_id = params[:client_id]
    comp_id = params[:component_id]
    if id.blank?
      client = Client.find_by(id: client_id)
    else
      client = Client.find_by(id: id)
    end
    value = false
    perm = Permission.find_by(client_id: client.id, user_id: current_user)
    if !perm.blank?
        value = true
    end
    value
  end

  def is_editor
    redirect_to(root_url) unless editor_clients | current_user.admin?
  end
end
