class PermissionsController < ApplicationController


  before_action :editor_permission, only: [:new, :create, :destroy]

  def new
    @permission = Permission.new
  end

  def create
    @permission = Permission.new(permission_params)
    respond_to do |format|
      if @permission.save
        format.html { redirect_to :back, notice: 'Parameter was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def destroy
    @permission = Permission.find(params[:id]).destroy
    flash[:success] = "Permission deleted"
    redirect_to :back
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def permission_params
    params.require(:permission).permit(:name, :value, :client_id, :user_id, :component_id)
  end

end
