class AssociatedUsersController < ApplicationController

  before_action :is_editor_comp, only: [:new, :create]
  before_action :editor_assoc, only: [:edit, :update, :destroy]
  autocomplete :ad_user, :name, :display_value => :auto_display, :extra_data => [:username, :password,
                                                                                  :active_directory]

  # GET /parameters/new
  def new
    @associated_user = AssociatedUser.new
  end

  # GET /parameters/1/edit
  def edit
    @associated_user = AssociatedUser.find(params[:id])
  end

  # POST /parameters
  def create
    @component = Component.find(params[:component_id])
    @associated_user = @component.associated_users.create(associated_user_params)
    respond_to do |format|
      if @associated_user.save
        format.html { redirect_to @component, notice: 'Comment was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  # PATCH/PUT /parameters/1
  def update
    @associated_user = AssociatedUser.find(params[:id])
    if @associated_user.update_attributes(associated_user_params)
      flash[:success] = "Updated"
      redirect_to @associated_user.component
    else
      render 'edit'
    end
  end

  # DELETE /parameters/1
  def destroy
    @associated_user = AssociatedUser.find(params[:id]).destroy
    @component = @associated_user.component
    flash[:success] = "Component deleted"
    redirect_to :back
  end

  # def autocomplete_ad_user_name
  #   term = params[:term]
  #   client_id = params[:client_id]
  #   ad_users = AdUser.where('client_id = ? AND name LIKE ?', client_id, "%#{term}%").order(:name)
  #                  .all
  #   render :json => ad_users.map {|ad_user| {:id => ad_user.id, :name => ad_user.name, :username => ad_user.username,
  #    :password => ad_user.password, :active_directory => ad_user.active_directory      } }
  # end


  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def associated_user_params
      params.require(:associated_user).permit(:name, :username, :password, :component_id)
    end
end
