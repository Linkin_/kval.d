class ComponentsController < ApplicationController

	before_action :user_access, only: [:show] #defining user rights
  before_action :is_editor, only: [:new, :create]
  before_action :is_editor_comp, only: [:new_child, :create_child, :edit, :update, :destroy]

  def new
    client = Client.find(params[:client_id])
    @component = client.components.build
  end

  # This function used when creating nested component
  def new_child
    comp = Component.find(params[:parent_id])
    @component = comp.children.create
  end

  def show
    @component = Component.find(params[:id])
    @components = @component.hash_tree
    @permissions = Permission.find_by_component_id(@component.id)
    @ancestors = @component.self_and_ancestors_ids
  end

  def create
    client = Client.find(params[:client_id])
    @component = client.components.create(component_params)
    if @component.save
      flash[:success] = "Component created"
      redirect_to  @component
    else
      render 'new'
    end
  end

  # This function used when creating nested component
  def create_child
    comp = Component.find(params[:parent_id])
    @component = comp.children.create(component_params)
    if @component.save
      flash[:success] = "Component created"
      redirect_to  @component
    else
      render 'new_child'
    end
  end


  def update
    @component = Component.find(params[:id])
    if @component.update_attributes(component_params)
      flash[:success] = "Updated"
      redirect_to @component
    else
      render 'edit'
    end
  end

  def edit
    @component = Component.find(params[:id])
  end

  def destroy
    Component.find(params[:id]).destroy
    flash[:success] = "Component deleted"
    redirect_to :back
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def component_params
    params.require(:component).permit(:name,:client_id,:parent_id, :descr)
  end

end

