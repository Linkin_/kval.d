class UsersController < ApplicationController
  before_action :correct_user, only: [:edit, :update, :show]
  before_action :admin_user,     only: [:destroy, :create, :new, :index]

  def index
    @users = User.paginate(page: params[:page], :per_page => 10)
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Registration was successfull"
      redirect_to  users_url
    else
      render 'new'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end


  private

  def user_params
    if current_user.admin?
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :admin)
    else
        params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
    end
    end

end
