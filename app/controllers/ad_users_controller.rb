class AdUsersController < ApplicationController

  before_action :is_editor,     only: [:new,:create, :edit,:update, :destroy]


  def new
    client = Client.find(params[:client_id])
    @ad_user = client.ad_users.build
  end


  def edit
    @ad_user = AdUser.find(params[:id])
  end


  def create
    client = Client.find(params[:client_id])
    @ad_user = client.ad_users.create(ad_user_params)
    if @ad_user.save
      flash[:success] = "AD user created"
      redirect_to  @ad_user.client
    else
      render 'new'
    end
  end


  def update
    @ad_user = AdUser.find(params[:id])
    if @ad_user.update_attributes(ad_user_params)
      flash[:success] = "Updated"
      redirect_to @ad_user.client
    else
      render 'edit'
    end
  end


  def destroy
    AdUser.find(params[:id]).destroy
    flash[:success] = "AD user deleted"
    redirect_to clients_url
  end

  private


  def ad_user_params
    params.require(:ad_user).permit(:name, :username, :password, :active_directory,:user_id, :client_id)
  end
end
