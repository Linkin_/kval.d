class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include ComponentsHelper
  include ClientsHelper
  include ParametersHelper
  include AssociatedUsersHelper
  include PermissionsHelper
  before_action :logged_in_user

  private
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to root_url
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)||current_user.admin?
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end


def editor
  redirect_to(root_url) unless editor_clients || editor_comp
end