class SessionsController < ApplicationController
  before_action :logged_in_user, only: [:root]

  # Initialize new session if there is not saved cookies for this user
  def new
    if current_user
      redirect_to current_user
    end
  end

  # Create new session
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      #if user is find and authenticated then log in
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to user
    else
      # Create an error message.
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
