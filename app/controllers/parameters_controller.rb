class ParametersController < ApplicationController
  before_action :is_editor_comp, only: [:new, :create]
  before_action :editor_param, only: [:edit, :update, :destroy]


  def new
    @parameter = Parameter.new
  end

  def edit
    @parameter = Parameter.find(params[:id])
  end

  def create
    @component = Component.find(params[:component_id])
    @parameter = @component.parameters.create(parameter_params)
    respond_to do |format|
      if @parameter.save
        format.html { redirect_to @component, notice: 'Parameter was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def update
    @parameter = Parameter.find(params[:id])
    if @parameter.update_attributes(parameter_params)
      flash[:success] = "Updated"
      redirect_to @parameter.component
    else
      render 'edit'
    end
  end

  def destroy
    @parameter = Parameter.find(params[:id]).destroy
    @component = @parameter.component
    flash[:success] = "Parameter deleted"
    redirect_to @component
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def parameter_params
      params.require(:parameter).permit(:name, :value, :component_id)
    end
end
