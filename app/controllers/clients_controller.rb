class ClientsController < ApplicationController
  before_action :is_editor,      only: [:show,:edit, :update]
  before_action :admin_user, only: [:new, :create, :update, :index]

  def index
    @clients = Client.paginate(page: params[:page], :per_page => 10)
  end

  def show
    @client = Client.find(params[:id])
    @ad_users = @client.ad_users.paginate(page: params[:ad_users_page], :per_page => 10)
    @components = @client.components.paginate(page: params[:components_page], :per_page => 10)
  end

  def new
    @client = Client.new
  end

  def edit
    @client = Client.find(params[:id])
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      flash[:success] = "Client created"
      redirect_to  clients_url
    else
      render 'new'
    end
  end

  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(client_params)
      flash[:success] = "Profile updated"
      redirect_to @client
    else
      render 'edit'
    end
  end

  def destroy
    Client.find(params[:id]).destroy
    flash[:success] = "Client deleted"
    redirect_to clients_url
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:name, :descr)
    end
end
