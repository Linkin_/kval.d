Rails.application.routes.draw do

  resources :clients, shallow: true do
    resources :ad_users, :except => [:index, :show]
    resources :components, :except => [:index] do
      resources :parameters, :except => [:index, :show]
      resources :associated_users, :except => [:index, :show] do
        get :autocomplete_ad_user_name, :on => :collection
      end
    end
  end

  resources :users
  resources :permissions, :only => [:new, :create, :destroy]
  post   'client_new'=> 'clients#new'
  get    'client_new'=> 'clients#new'
  post   '/components/:component_id/permissions/new' => 'permissions#create', as: :component_permission
  post   '/clients/:client_id/permissions/new' => 'permissions#create', as: :client_permission
  get    '/components/:parent_id/new' => 'components#new_child', as: :new_component
  post   '/components/:parent_id/new' => 'components#create_child', as: :create_component
  #post   'client_new'     => 'clients#new'
  get    'signup'  => 'users#new'
  post   'signup'  => 'users#new'
  root                 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  match '*path' => redirect('/'), via: [:get,:post]   unless Rails.env.development?

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
