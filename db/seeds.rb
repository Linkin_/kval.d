# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "Roberts Osipovs",
             email: "r.osipovs@scandicfusion.com",
             password:              "password",
             password_confirmation: "password",
             admin: true)

25.times do |n|
  name  = Faker::Name.name
  email = "user-#{n+1}@scf.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

12.times do |n|
  name  = Faker::Company.name
  descr = Faker::Lorem.sentence(20)
  Client.create!(name:  name,
               descr: descr)
end

clients = Client.all
12.times do |n|
  name = Faker::Name.name
  user = "user#{n+1}"
  pass = "pass"
  active_directory =  "AD"
  clients.each {|client| client.ad_users.create!(name: name, username: user, password: pass, active_directory:
                                                               active_directory)}
end

clients = Client.all
8.times do |n|
  name = Faker::Internet.domain_name
  descr = Faker::Lorem.sentence(40)
  clients.each {|client| client.components.create!(name: name)}
end

comp = Component.all
5.times do |n|
  name = Faker::Internet.domain_name
  descr = Faker::Lorem.sentence(40)
  comp.each {|comp| comp.children.create!(name: name, descr: descr)}
end

comp = Component.leaves
2.times do |n|
  name = Faker::Internet.domain_name
  descr = Faker::Lorem.sentence(40)
  comp.each {|comp| comp.children.create!(name: name, descr: descr)}
end