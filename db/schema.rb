# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150521091552) do

  create_table "ad_users", force: :cascade do |t|
    t.text     "name"
    t.text     "username"
    t.text     "password"
    t.text     "active_directory"
    t.integer  "user_id"
    t.integer  "client_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "ad_users", ["client_id"], name: "index_ad_users_on_client_id"
  add_index "ad_users", ["user_id", "client_id", "name"], name: "index_ad_users_on_user_id_and_client_id_and_name"
  add_index "ad_users", ["user_id"], name: "index_ad_users_on_user_id"

  create_table "associated_users", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.string   "password"
    t.integer  "component_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "associated_users", ["component_id"], name: "index_associated_users_on_component_id"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.text     "descr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "clients", ["id"], name: "index_clients_on_id"

  create_table "component_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
  end

  add_index "component_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "component_anc_desc_idx", unique: true
  add_index "component_hierarchies", ["descendant_id"], name: "component_desc_idx"

  create_table "components", force: :cascade do |t|
    t.text     "name"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
    t.string   "descr"
  end

  add_index "components", ["client_id"], name: "index_components_on_client_id"
  add_index "components", ["parent_id"], name: "index_components_on_parent_id"

  create_table "parameters", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.integer  "component_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "parameters", ["component_id"], name: "index_parameters_on_component_id"

  create_table "permissions", force: :cascade do |t|
    t.integer  "value"
    t.integer  "user_id"
    t.integer  "client_id"
    t.integer  "component_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "permissions", ["client_id"], name: "index_permissions_on_client_id"
  add_index "permissions", ["component_id"], name: "index_permissions_on_component_id"
  add_index "permissions", ["user_id"], name: "index_permissions_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
