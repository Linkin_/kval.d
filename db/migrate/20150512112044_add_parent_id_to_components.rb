class AddParentIdToComponents < ActiveRecord::Migration
  def change
    add_column :components, :parent_id, :integer
    add_column :components, :descr, :string
    add_index :components, [:parent_id]
  end
end
