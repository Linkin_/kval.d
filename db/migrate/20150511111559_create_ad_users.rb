class CreateAdUsers < ActiveRecord::Migration
  def change
    create_table :ad_users do |t|
      t.text :name
      t.text :username
      t.text :password
      t.text :active_directory
      t.references :user, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :ad_users, [:user_id, :client_id, :name]
  end
end
