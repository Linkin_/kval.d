class CreateParameters < ActiveRecord::Migration
  def change
    create_table :parameters do |t|
      t.string :name
      t.string :value
      t.references :component, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
