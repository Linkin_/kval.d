class CreateAssociatedUsers < ActiveRecord::Migration
  def change
    create_table :associated_users do |t|
      t.string :name
      t.string :username
      t.string :password
      t.references :component, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
