class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.text :descr

      t.timestamps null: false
    end
    add_index :clients, [:id]
  end
end
