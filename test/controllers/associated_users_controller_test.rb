require 'test_helper'

class AssociatedUsersControllerTest < ActionController::TestCase
  setup do
    @associated_user = associated_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:associated_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create associated_user" do
    assert_difference('AssociatedUser.count') do
      post :create, associated_user: { Component_id: @associated_user.Component_id, name: @associated_user.name, password: @associated_user.password, string: @associated_user.string, username: @associated_user.username }
    end

    assert_redirected_to associated_user_path(assigns(:associated_user))
  end

  test "should show associated_user" do
    get :show, id: @associated_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @associated_user
    assert_response :success
  end

  test "should update associated_user" do
    patch :update, id: @associated_user, associated_user: { Component_id: @associated_user.Component_id, name: @associated_user.name, password: @associated_user.password, string: @associated_user.string, username: @associated_user.username }
    assert_redirected_to associated_user_path(assigns(:associated_user))
  end

  test "should destroy associated_user" do
    assert_difference('AssociatedUser.count', -1) do
      delete :destroy, id: @associated_user
    end

    assert_redirected_to associated_users_path
  end
end
